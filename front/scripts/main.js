 (function($)
{
	$('.dropdown')
	  .dropdown({
	    // you can use any ui transition
	    transition: 'drop'
	  });

	  $('#sidebar-menu .ui.sticky')
	  .sticky({
	    context: '#main-content',
	    offset: 35
	  });

	  $('.ui.sidebar')
		  .sidebar({
		    context: $('.bottom.segment')
		  })
		  .sidebar('attach events', '.menu .item');

	  // auto show top menu
	  var headerFixedFunc = function()
	  {	
	  	var headerFixed = $('#header-fixed-menu');
	  	if(window.pageYOffset >= 68 && headerFixed.is(':hidden'))
	  	{
	  		headerFixed
	  		.transition('slide down');
	  	} else if(window.pageYOffset < 68 && headerFixed.is(':visible')) {
	  		headerFixed
			.transition('slide down');
	  	}
	  };
	  $(window).bind('scroll load', _.debounce(headerFixedFunc, 300));
})(jQuery);